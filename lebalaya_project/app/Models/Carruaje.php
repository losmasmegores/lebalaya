<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Carruaje extends Model
{
    use HasFactory;
    protected $fillable = ['marca', 'model', 'any', 'preu', 'kilometres', 'imatge'];

    public function publi()
    {
        return $this->hasOne(Publicacao::class);
    }
}
