<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Publicacao extends Model
{
    use HasFactory;
    public $timestamps = true;

    protected $fillable = ['titulo', 'descripcion'];

    // one to many inversa (el lado one --> belongsTo)
    public function user(){
        return $this->belongsTo(User::class);
    }

    // one to many (el lado many --> hasMany)
    public function comments(){
        return $this->hasMany(Opinião::class);
    }

    // one to one
    public function car()
    {
        return $this->hasOne(Carruaje::class);
    }

}
