<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Opinião extends Model
{
    use HasFactory;
    public $timestamps = true;
    protected $fillable = ['text'];

    public function publicacio()
    {
        return $this->belongsTo(Publicacao::class);
    }
    public function usuario()
    {
        return $this->belongsTo(User::class);
    }
}
