<?php
class UserController extends Controller
{
    use AuthorizesRequests, ValidatesRequests;

    //Mostra la pàgina principal amb totes les ofertes de cotxes.
    public function index()
    {
        return view('index')->with('posts', Post::all());
    }

    // Mostra el formulari per crear una nova oferta.
    public function showNewAddForm()
    {
        return view('newAdd');
    }

    //Mostra les ofertes publicades per l'usuari.
    public function showAdds()
    {
        $posts = \App\Models\Publicacao::all();
        $publicacions = array();
        foreach ($posts as $post) {
            if ($post->user_id == Auth::id()) {
                array_push($publicacions, $post);
            }
        }
        return view('index')->with('posts', $publicacions);
    }

    public function showSaved()
    {
    }



    // Emmagatzema la nova oferta creada per l'usuari.
    public function storeNewAdd(Request $req)
    {
        $coche = new \App\Models\Carruaje();
        $coche->marca = $req->marca;
        $coche->model = $req->model;
        $coche->any = $req->any;
        $coche->preu = $req->preu;
        $coche->kilometres = $req->kilometres;
        $coche->imatge = $req->imatge;
        $coche->save();

        $post = new \App\Models\Publicacao();
        $post->titulo = $req->titulo;
        $post->descripcion = $req->descripcion;
        $post->car_id = $coche->id;
        $post->user_id = Auth::id();
        $post->save();

        print($req->marca);
        //ESTO SE PUEDE BORRAR SI DA PROBLEMAS
        return Redirect::to('/');
    }

    // Emmagatzema el nou comentari creat per l’usuari a la publicació seleccionada.
    public function newComment()
    {
    }
}
