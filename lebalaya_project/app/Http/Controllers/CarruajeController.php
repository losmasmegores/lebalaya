<?php

class CarController extends Controller
{
    use AuthorizesRequests, ValidatesRequests;

    // Mostra la pàgina principal amb totes les ofertes de cotxes.
    public function index()
    {
        $carruajes = \App\Models\Carruaje::all();
        return $carruajes;
    }
}
