<?php

namespace Database\Seeders;

use App\Models\Opinião;
use Illuminate\Database\Seeder;

class OpiniaoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Opinião::create([
            'text' => '¡Me encanta este sitio web! Es tan genial y moderno, ¡es como si estuviera en el futuro! 😍',
        ]);

        Opinião::create([
            'text' => 'Este es el mejor producto que he comprado en mi vida. ¡Nunca volveré a ser el mismo después de usarlo! 😎',
        ]);

    }
}
