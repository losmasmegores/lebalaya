<?php

namespace Database\Seeders;

use App\Models\Publicacao;
use Illuminate\Database\Seeder;

class PublicacaoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {

        Publicacao::create([
            'titulo' => 'La conspiración de los gatos extraterrestres',
            'descripcion' => '¡Acabo de descubrir que los gatos son en realidad extraterrestres disfrazados que controlan nuestras mentes mientras dormimos! 😱👽',
        ]);

        Publicacao::create([
            'titulo' => 'El misterio de las zanahorias cantantes',
            'descripcion' => 'Hoy presencié algo asombroso: las zanahorias en mi nevera comenzaron a cantar en coro. ¡Debo investigar este fenómeno vegetal! 🥕🎶',
        ]);

    }
}
