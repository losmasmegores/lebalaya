<?php

namespace Database\Seeders;

use App\Models\Carruaje;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Car;

class CarruajeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Carruaje::create([
            'brand' => 'Rpm',
            'model' => 'mateArgentino',
            'year' => 7777,
            'price' => 2,
            'km' => 22,
            'image' => 'public/mateargentino.png'
        ]);
        Carruaje::create([
            'brand' => 'MorciQuiz',
            'model' => 'autodelicious',
            'year' => 0,
            'price' => 20000,
            'km' => 2,
            'image' => 'public/autodelicioso.png'
        ]);


    }
}
