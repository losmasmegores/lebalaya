<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {

        // Agregar nuevos usuarios con nombres cringe y contraseñas cringe
        User::create([
            'name' => 'DiegitoUchihaStormWarrior222',
            'email' => 'diegito@example.com',
            'password' => Hash::make('shinobi123'),
        ]);

        User::create([
            'name' => 'KawaiiDesuSenpai666',
            'email' => 'kawaii@example.com',
            'password' => Hash::make('uwuGatito123'),
        ]);

    }
}
